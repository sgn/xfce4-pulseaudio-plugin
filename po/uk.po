# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Andrii Protsun <minamotosoft@gmail.com>, 2020
# Gordon Freeman, 2023
# Yarema aka Knedlyk <yupadmin@gmail.com>, 2015,2017-2018
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-09-05 00:50+0200\n"
"PO-Revision-Date: 2015-10-05 20:18+0000\n"
"Last-Translator: Gordon Freeman, 2023\n"
"Language-Team: Ukrainian (http://app.transifex.com/xfce/xfce-panel-plugins/language/uk/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: uk\n"
"Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);\n"

#: ../panel-plugin/pulseaudio.desktop.in.in.h:1
msgid "PulseAudio Plugin"
msgstr "Додаток PulseAudio"

#: ../panel-plugin/pulseaudio.desktop.in.in.h:2
#: ../panel-plugin/pulseaudio-plugin.c:256
msgid "Adjust the audio volume of the PulseAudio sound system"
msgstr "Налаштувати гучність аудіо звукової системи PulseAudio"

#: ../panel-plugin/pulseaudio-dialog.glade.h:1
msgid "None"
msgstr "Нічого"

#: ../panel-plugin/pulseaudio-dialog.glade.h:2
msgid "All"
msgstr "Усе"

#: ../panel-plugin/pulseaudio-dialog.glade.h:3
msgid "Output only"
msgstr "Лише вивід"

#: ../panel-plugin/pulseaudio-dialog.glade.h:4
msgid "Input only"
msgstr "Лише ввід"

#: ../panel-plugin/pulseaudio-dialog.glade.h:5
msgid "PulseAudio Panel Plugin"
msgstr "Додаток панелі PulseAudio"

#: ../panel-plugin/pulseaudio-dialog.glade.h:6
msgid "Enable keyboard _shortcuts for volume control"
msgstr "Включити _скорочення клавіатури для контролю гучності"

#: ../panel-plugin/pulseaudio-dialog.glade.h:7
msgid ""
"Enables volume control using multimedia keys. Make sure no other application"
" that listens to these keys (e.g. xfce4-volumed) is running in the "
"background."
msgstr "Включити контроль гучності, використовуючи клавіші мультимедіа. Впевніться, що жодна інша програма, яка використовує ці клавіші (наприклад xfce4-volumed), не працює в тлі."

#: ../panel-plugin/pulseaudio-dialog.glade.h:8
msgid "Play system _sound when volume changes"
msgstr "_Програвати системний звук при зміні гучності"

#: ../panel-plugin/pulseaudio-dialog.glade.h:9
msgid ""
"Enables audio feedback when using multimedia keys to change the volume."
msgstr "Вмикає аудіосповіщення під час використання мультимедійних клавіш для зміни гучності."

#: ../panel-plugin/pulseaudio-dialog.glade.h:10
msgid "Always display the recording _indicator"
msgstr "Завжди відображати _індикатор запису"

#: ../panel-plugin/pulseaudio-dialog.glade.h:11
msgid "Show volume _notifications:"
msgstr "Показувати спо_віщення гучності"

#: ../panel-plugin/pulseaudio-dialog.glade.h:12
msgid "Mute/unmute is notified in all modes except \"None\""
msgstr "Сповіщення про вимкненні/увімкненні звуку в усіх режимах, крім «Нічого»"

#: ../panel-plugin/pulseaudio-dialog.glade.h:13
msgid "Step size used when adjusting volume with mouse wheel or hotkeys."
msgstr "Розмір кроку, який використовується під час регулювання гучності за допомогою колеса миші або гарячих клавіш."

#: ../panel-plugin/pulseaudio-dialog.glade.h:14
msgid "_Volume step:"
msgstr "_Крок гучності:"

#: ../panel-plugin/pulseaudio-dialog.glade.h:15
msgid "M_aximum volume:"
msgstr "Максимум _гучності:"

#: ../panel-plugin/pulseaudio-dialog.glade.h:16
msgid "Behaviour"
msgstr "Поведінка"

#: ../panel-plugin/pulseaudio-dialog.glade.h:17
msgid "Audio _Mixer"
msgstr "Аудіо _мікшер"

#: ../panel-plugin/pulseaudio-dialog.glade.h:18
msgid ""
"Audio mixer command that can be executed from the context menu, e.g. "
"\"pavucontrol\", \"unity-control-center sound\"."
msgstr "Команди аудіо-мікшера, які можна запустити з контекстного меню, наприклад \"pavucontrol\", "

#: ../panel-plugin/pulseaudio-dialog.glade.h:19
msgid "_Run Audio Mixer..."
msgstr "_Запустити Аудіо мікшер..."

#: ../panel-plugin/pulseaudio-dialog.glade.h:20
msgid "Sound Settings"
msgstr "Налаштування звуку"

#: ../panel-plugin/pulseaudio-dialog.glade.h:21
msgid "General"
msgstr "Основне"

#: ../panel-plugin/pulseaudio-dialog.glade.h:22
msgid "Control Playback of Media Players"
msgstr "Контроль програвання медіа-програвачів"

#: ../panel-plugin/pulseaudio-dialog.glade.h:23
msgid "Enable multimedia keys for playback control"
msgstr "Включити мультимедійні кнопки для контролю програвання"

#: ../panel-plugin/pulseaudio-dialog.glade.h:24
msgid "Send multimedia keys to all players"
msgstr "Надсилати мультимедійні клавіші у всі плеєри"

#: ../panel-plugin/pulseaudio-dialog.glade.h:25
msgid "Enable experimental window focus support"
msgstr "Включити експериментальну підтримку фокусу вікна"

#: ../panel-plugin/pulseaudio-dialog.glade.h:26
msgid "Name"
msgstr "Назва"

#: ../panel-plugin/pulseaudio-dialog.glade.h:27
msgid "Persistent"
msgstr "Постійний"

#: ../panel-plugin/pulseaudio-dialog.glade.h:28
msgid "Ignored"
msgstr "Ігноруємий"

#: ../panel-plugin/pulseaudio-dialog.glade.h:29
msgid "Clear Known Items"
msgstr "Очистити відомі елементи"

#: ../panel-plugin/pulseaudio-dialog.glade.h:30
msgid "Please restart the players to make them visible again."
msgstr "Будь ласка, перезапустіть програвачі, щоб зробити їх знову видимими."

#: ../panel-plugin/pulseaudio-dialog.glade.h:31
msgid "Known Media Players"
msgstr "Відому медіа-програвачі"

#: ../panel-plugin/pulseaudio-dialog.glade.h:32
msgid "Media Players"
msgstr "Медіа-програвачі"

#: ../panel-plugin/pulseaudio-dialog.c:142
#: ../panel-plugin/pulseaudio-menu.c:251
#, c-format
msgid ""
"<big><b>Failed to execute command \"%s\".</b></big>\n"
"\n"
"%s"
msgstr "<big><b>Помилка запуску команди: \"%s\".</b></big>\n\n%s"

#: ../panel-plugin/pulseaudio-dialog.c:145
#: ../panel-plugin/pulseaudio-menu.c:254
msgid "Error"
msgstr "Помилка"

#: ../panel-plugin/pulseaudio-button.c:339
#: ../panel-plugin/pulseaudio-notify.c:199
#, c-format
msgid "Not connected to the PulseAudio server"
msgstr "Немає з’єднання з сервером "

#: ../panel-plugin/pulseaudio-button.c:357
#, c-format
msgid ""
"<b>Volume %d%% (muted)</b>\n"
"<small>%s</small>"
msgstr "<b>Гучність %d%% (заглушено)</b>\n<small>%s</small>"

#: ../panel-plugin/pulseaudio-button.c:359
#, c-format
msgid ""
"<b>Volume %d%%</b>\n"
"<small>%s</small>"
msgstr "<b>Гучність %d%%</b>\n<small>%s</small>"

#: ../panel-plugin/pulseaudio-menu.c:521
msgid "Output"
msgstr "Вивід"

#: ../panel-plugin/pulseaudio-menu.c:561
msgid "Input"
msgstr "Ввід"

#: ../panel-plugin/pulseaudio-menu.c:663
msgid "Choose Playlist"
msgstr "Вибір списку програвання"

#. Audio mixers
#: ../panel-plugin/pulseaudio-menu.c:698
msgid "_Audio mixer..."
msgstr "_Аудіо мікшер..."

#: ../panel-plugin/pulseaudio-notify.c:201
#, c-format
msgid "Volume %d%c (muted)"
msgstr "Гучність %d%c (заглушено)"

#: ../panel-plugin/pulseaudio-notify.c:203
#, c-format
msgid "Volume %d%c"
msgstr "Гучність %d%c"

#: ../panel-plugin/mprismenuitem.c:374 ../panel-plugin/mprismenuitem.c:447
#: ../panel-plugin/mprismenuitem.c:802
msgid "Not currently playing"
msgstr "Зараз не програється"
